"use strict";

function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

/*  $(function() {
    $(window).scroll(function() {
      if($(this).scrollTop() != 0) {
        $('.btn-up').fadeIn();
      } else {
        $('.btn-up').fadeOut();
      }
    });
    $('.btn-up').click(function() {
      $('body,html').animate({scrollTop:0},1500);
    });
  }); */

/* $(document).ready(function () {

	//burger
	if(document.querySelector('.js-burger')){
		$('.js-burger').click(function(){
			if($(this).hasClass('active')){
				$(this).removeClass('active');
				// $('.js-mask').fadeOut('active');
				$('.js-burger-menu').removeClass('active');
				$('.main').removeClass('opacity');
				$('body').removeClass('overflow');
				$('html').removeClass('overflow');
			} else {
				$(this).addClass('active');
				// $('.js-mask').fadeIn('active');
				$('.js-burger-menu').addClass('active');
				$('.main').addClass('opacity');
				$('body').addClass('overflow');
				$('html').addClass('overflow');
			}
		})
	}

}); */

/* $(document).ready(function () {
  $(".header-btn1").click(function () {
    $(".modal").fadeIn(500);
  });
  $(".modal").on("click", function (e) {
    if (e.target.id == "modal") {
      $(this).hide();
    }
  }); */

/* $('#but2').click(function(){
	  $('#form2').show(300);
	}); */

/* });
$(document).ready(function () {
  $(".form-input3").click(function () {
    $(".modal2").fadeIn(500);
  });
  $(".modal2").on("click", function (e) {
    if (e.target.id == "modal2") {
      $(this).hide();
    }
  });
});
$(document).ready(function () {
  $(".form-input4").click(function () {
    $(".modal3").fadeIn(500);
  });
  $(".modal3").on("click", function (e) {
    if (e.target.id == "modal3") {
      $(this).hide();
    }
  });
});
$(document).ready(function () {
  $(".form-input6").click(function () {
    $(".modal4").fadeIn(500);
  });
  $(".modal4").on("click", function (e) {
    if (e.target.id == "modal4") {
      $(this).hide();
    }
  });
});
$(document).ready(function () {
  $(".header-btn2").click(function () {
    $(".modal5").fadeIn(500);
  });
  $(".modal5").on("click", function (e) {
    if (e.target.id == "modal5") {
      $(this).hide();
    }
  });
});
$(document).ready(function () {
  $(".form5-input3").click(function () {
    $(".modal6").fadeIn(500);
  });
  $(".modal6").on("click", function (e) {
    if (e.target.id == "modal6") {
      $(this).hide();
    }
  });
}); */

/*  $(function() {
    $(window).scroll(function() {
      if($(this).scrollTop() != 0) {
        $('.btn-up').fadeIn();
      } else {
        $('.btn-up').fadeOut();
      }
    });
    $('.btn-up').click(function() {
      $('body,html').animate({scrollTop:0},1500);
    });
  }); */

/*  $(function() {
    $(window).scroll(function() {
      if($(this).scrollTop() != 0) {
        $('.btn-up').fadeIn();
      } else {
        $('.btn-up').fadeOut();
      }
    });
    $('.btn-up').click(function() {
      $('body,html').animate({scrollTop:0},1500);
    });
  }); */
$(document).ready(function () {
  var anchors = document.querySelectorAll("a.scroll-link");

  var _iterator = _createForOfIteratorHelper(anchors),
      _step;

  try {
    var _loop = function _loop() {
      var anchor = _step.value;
      anchor.addEventListener("click", function (e) {
        e.preventDefault();
        var blockID = anchor.getAttribute("href").substr(1);
        $(".js-nav").removeClass("active");
        $(".js-burger").removeClass("active");
        $("body").removeClass("overflow");
        document.getElementById(blockID).scrollIntoView({
          behavior: "smooth",
          block: "start"
        });
      });
    };

    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      _loop();
    } //input parametr

  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }

  $("input.basic-input").click(function () {
    $(this).parents(".package").removeClass("premium");
  });
  $("input.premium-input").click(function () {
    $(this).parents(".package").addClass("premium");
  });
});